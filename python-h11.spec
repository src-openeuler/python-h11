%bcond_without tests

%global _empty_manifest_terminate_build 0
Name:		python-h11
Version:	0.14.0
Release:	1
Summary:	A pure-Python, bring-your-own-I/O implementation of HTTP/1.1
License:	MIT
URL:		https://github.com/python-hyper/h11
Source0:    https://github.com/python-hyper/h11/archive/refs/tags/v%{version}.tar.gz#/h11-%{version}.tar.gz
BuildArch:	noarch


%description
h11 is suitable for implementing both servers and clients, and has a
pleasantly symmetric API: the events you send as a client are exactly
the ones that you receive as a server and vice-versa.

%package -n python3-h11
Summary:	A pure-Python, bring-your-own-I/O implementation of HTTP/1.1
Provides:	python-h11
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%if %{with tests}
BuildRequires: python3-pytest
%endif
%description -n python3-h11
h11 is suitable for implementing both servers and clients, and has a
pleasantly symmetric API: the events you send as a client are exactly
the ones that you receive as a server and vice-versa.

%package help
Summary:	Development documents and examples for h11
Provides:	python3-h11-doc
%description help
h11 is suitable for implementing both servers and clients, and has a
pleasantly symmetric API: the events you send as a client are exactly
the ones that you receive as a server and vice-versa.

%prep
%autosetup -n h11-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%if %{with tests}
%check
py.test-%{python3_version} --verbose
%endif

%files -n python3-h11 -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Fri Sep 30 2022 jiangxinyu <jiangxinyu@kylinos.cn> - 0.14.0-1
- Upgrade to 0.14.0

* Fri Jul 23 2021 Python_Bot <Python_Bot@openeuler.org> - 0.12.0-1
- Package Spec generated
